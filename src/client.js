const getRipeness = (name) => {
    const refinement = name.match(/(?<=\[\s*).*?(?=\s*\])/g);

    let ripeness = {
        icon: 'https://raw.githubusercontent.com/encharm/Font-Awesome-SVG-PNG/master/white/png/64/train.png',
    };

    if(refinement.length == 0) {
        return {};
    }

    let value = parseInt(refinement[0]);;

    if(isNaN(value)) {
        return {
            icon: 'https://raw.githubusercontent.com/encharm/Font-Awesome-SVG-PNG/master/white/png/64/train.png',
            color: 'red',
        };
    }

    switch(value) {
        case 0:
            ripeness.color = 'red';
            break;
        case 1:
            ripeness.color = 'yellow';
            break;
        case 2:
            ripeness.color = 'green';
            break;
        default:
            ripeness.color = 'green';
            break;
    }

    return ripeness;
};

const getStars = (name) => {
    const stars = name.match(/\*/g);

    let starColor = 'green';

    switch(stars.length) {
        case 1:
            starColor = 'green';
            break;
        case 2:
            starColor = 'yellow';
            break;
        default:
            starColor = 'red';
            break;

    }

    return {
        icon: 'https://raw.githubusercontent.com/encharm/Font-Awesome-SVG-PNG/master/white/png/64/star.png',
        color: starColor,
    };
};

const getEstimations = (name) => {
    const est = name.match(/(?<=\(\s*).*?(?=\s*\*)/g);

    let value = parseFloat(est[0].slice(0, -1));

    if(isNaN(value)) {
        return {};
    }

    let estimation = {
        icon: 'https://raw.githubusercontent.com/encharm/Font-Awesome-SVG-PNG/master/white/png/64/clock-o.png',
        color: 'green'
    };

    if(value >= 3) {
        estimation.color = 'red';
    } else if(value >= 2) {
        estimation.color = 'yellow';
    }

    return estimation;
};

const cardBadges = async (t) => {
    const card = await t.card('name');

    const estimation = getEstimations(card.name);
    const stars = getStars(card.name);
    const ripeness = getRipeness(card.name);

    return [estimation, stars, ripeness];
};

TrelloPowerUp.initialize({
    'card-badges': cardBadges,
    'card-detail-badges': cardBadges,
});
